package com.example.appmicalculadora

class Operaciones(var num1:Float, var num2:Float){

    public fun suma() : Float{
        return this.num1 + this.num2;
    }
    public fun rest() : Float{
        return this.num1 - this.num2;
    }
    public fun mult() : Float{
        return this.num1 * this.num2;
    }
    public fun divi() : Float{
        return this.num1 / this.num2;
    }
}