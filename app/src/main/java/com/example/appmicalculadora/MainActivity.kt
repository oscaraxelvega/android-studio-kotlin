package com.example.appmicalculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    private lateinit var txtUsuario:EditText;
    private lateinit var txtContraseña:EditText;
    private lateinit var btnIngresar:Button;
    private lateinit var btnSalir:Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        initComponents();
        clickEvents();
    }
    //inicializar componentes
    public fun initComponents(){
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    public fun clickEvents(){
        btnIngresar.setOnClickListener(View.OnClickListener {
            var user:String = getString(R.string.usuario);
            var pass:String = getString(R.string.pass);
            var nombre:String = getString(R.string.nombre);

            if(txtUsuario.text.toString().contentEquals(user)
                && txtContraseña.text.toString().contentEquals(pass)){
                val intent = Intent(this,OperacionesActivity::class.java);
                intent.putExtra("nombre",nombre);
                startActivity(intent);
            } else {
                Toast.makeText(this,"Datos faltantes o incorrectos",Toast.LENGTH_SHORT).show();
            }
        })
        btnSalir.setOnClickListener(View.OnClickListener {
            finish();
        })
    }
}